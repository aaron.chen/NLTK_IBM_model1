#!/usr/bin/python3

import sys
import nltk
import math

from IBM_vocabulary import Vocabulary
from IBM_conditional import Conditional

# Implementation of Parallel Corpus for IBM model 1
class ParallelCorpus:

	#Constructor for initially empty parallel corpus. Will contain list of sentences for both languages + vocabularies
	def __init__(self):
		# List of English sentences. Each sentence will be represented as a list of ints.
		self.e = list() 
		# List of foreign sentences  Each sentence will be represented as a list of ints.
		self.f = list() 
		# Initially empty vocabularies
		self.e_vocab = Vocabulary()
		self.f_vocab = Vocabulary()

	# Returns the number of sentence pairs that have been added to this parallel corpus
	def size(self):
		return len(self.e)

	# Returns the list of integers corresponding to the English sentence at the specified sentence index
	def get_e(self, sentence_index):
		return self.e[sentence_index]

	# Returns the list of integers corresponding to the foreign sentence at the specified sentence index
	def get_f(self, sentence_index):
		return self.f[sentence_index]

	# Adds English sentence info + foreign sentence info to the parallel corpus
	# Given a string representing an English sentence and a string representing a foreign sentence,
	# tokenize each string using nltk.word_tokenize, and use the appropriate vocabulary to convert each token to an int.
	#   
	# Append the list of integers (corresponding to the English sentence) to self.e
	# Append the list of integers (corresponding to the foreign sentence) to self.f
	def add(self, e, f):
		self.e.append([self.e_vocab.get_int(word) for word in nltk.word_tokenize(e)])
		self.f.append([self.f_vocab.get_int(word) for word in nltk.word_tokenize(f)])

	# Construct a unifiorm conditional distribution with the given name.
	# Initially t(e|f) = 1.0 / |F|
	def create_uniform_distribution(self, name):
		return Conditional(name, self.e_vocab, self.f_vocab, 1.0 / len(self.f_vocab.word2int))

	# Given a sentence index, a scaling factor epsilon, and a conditional distribution, calculate the conditional probability 
	# of the English sentence (at that sentence index) given the foreign sentence (at that sentence index)
	#
	# Uses the formula: (epsilon / l_f**l_e) sum_e [ sum_f [ t(e|f)]]
	def conditional_probability(self, sentence_index, epsilon, conditional):
		e = self.get_e(sentence_index)
		f = self.get_e(sentence_index)
		cond_sum = sum(conditional.get(j,i) for j in e for i in f)
		return cond_sum * (epsilon*1.0 / (len(f)**len(e)))

	# Given a conditional distribution and a scaling factor epsilon, calculate the perplexity of this parallel corpus.
	#
	# Uses the formula: -sum_s [lg(p(e_s|f_s))]
	def perplexity(self, epsilon, conditional):
		return -sum(math.log2(self.conditional_probability(s, epsilon, conditional)) for s in range(self.size()))			

#If run directly with a filename argument, initializes the parallel corpus with sentence pairs from the file (split by tab)
#otherwise adds some sample "sentences"
#creates a sample uniform distribution parallel corpus, and prints its info / it's perplexity
if __name__ == '__main__':
	
	corpus = ParallelCorpus()

	if len(sys.argv) > 1:
		f = open(sys.argv[1])
		for line in f:
			e,f = line.split("\t")
			corpus.add(e,f)

	else:

		corpus.add("the house", "das Haus")
		corpus.add("the book",  "das Buch")
		corpus.add("a book",    "ein Buch")

	epsilon = 0.01
	t = corpus.create_uniform_distribution("t")
	print(t)
	print()
	ppl = corpus.perplexity(epsilon, t)
	print(ppl)
