#!/usr/bin/python3

import sys
import nltk

from IBM_vocabulary import Vocabulary
from itertools import product 
from collections import OrderedDict

# Implementation of conditional probability model (with base probability - no calculation done yet) for vocabulary in IBM Model 1
class Conditional:

	#Constructor: 
	#For every pair of words from e_vocab, f_vocab,
	#store the value provided by initial_value
	#also stores a provided name
	def __init__(self, name, e_vocab, f_vocab, initial_value):
		self.name = name
		self.e_vocab = e_vocab
		self.f_vocab = f_vocab
		self.efdict = OrderedDict()
		for e_w,f_w in product(e_vocab.word2int, f_vocab.word2int):
			self.efdict[(e_w,f_w)] = initial_value

	# Given an integer index for a word from e and a word from f, return the corresponding value
	def get(self, e_i, f_i):
		return self.efdict[(self.e_vocab.get_word(e_i), self.f_vocab.get_word(f_i))]
	
	# Given an integer index for a word from e and a word from f, store the value provided
	def set(self, e_i, f_i, value):
		self.efdict[(self.e_vocab.get_word(e_i), self.f_vocab.get_word(f_i))] = value

	# Return a string representation of this object
	def __str__(self):
		printstr = ''
		for key, val  in self.efdict.items():
			printstr += "{}[{} | {}] = {}\n".format(self.name,key[0], key[1], val)
		return printstr

#given list of words, assign indices for each word in a vocbulary and return the vocabulary
def create_vocab(words):
	v = Vocabulary()
	for word in words:
		v.get_int(word)
	return v

#if run directly with 2 filename arguments, create native lang / foreign vocabulary from files respectively
#otherwise use sample sentences
#show sample conditional probability (no calculations done yet)
if __name__ == '__main__':
	if (len(sys.argv) > 2):
		f1 = open(sys.argv[1])
		raw1 = f1.read()
		words_e=nltk.word_tokenize(raw1)

		f2 = open(sys.argv[2])
		raw2 = f2.read()
		words_f=nltk.word_tokenize(raw2)

	else:
		words_e = nltk.word_tokenize("All human beings are born free and equal in dignity and rights. They are endowed with reason and conscience and should act towards one another in a spirit of brotherhood.")
		words_f = nltk.word_tokenize("Todos los seres humanos nacen libres e iguales en dignidad y derechos y, dotados como están de razón y conciencia, deben comportarse fraternalmente los unos con los otros.")

	e_v = create_vocab(words_e)
	f_v = create_vocab(words_f)
		
	count = Conditional("count", e_v, f_v, 0)
	print(count)
	print()
