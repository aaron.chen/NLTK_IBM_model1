#!/usr/bin/python3

import sys
import nltk

from IBM_vocabulary import Vocabulary
from IBM_conditional import Conditional
from IBM_parallelcorpus import ParallelCorpus
from itertools import product

# Implementation of IBM Model 1 translation model
class IBM_Model1:

	#initialize Model 1 using a parallel corpus with a uniform distribution
	def __init__(self, parallel_corpus):
		self.parallel_corpus = parallel_corpus
		self.t = parallel_corpus.create_uniform_distribution("t")
		
	#calculates for the normalization constants for a partiular English word in e
	#returns dictionary z that maps from each English word to normalization constant
	#Uses formula: z_e = sum_f(t(e|f))
	def compute_normalization(self, e_sentence, f_sentence):
		z = {e_i:sum(self.t.get(e_i,f_i) for f_i in f_sentence) for e_i in e_sentence}
		return z

	#Given the conditional probability counts, updates the conditional probabilities
	#using the provided sentences and the normalization constant for that sentence 
	#Uses formula c(e|f) = c(e|f) + t(e|f)/z_e
	def update_counts(self, e_sentence, f_sentence, counts, z):
		for e_i, f_i in product(e_sentence, f_sentence):
			counts.set(e_i, f_i, (counts.get(e_i,f_i) + self.t.get(e_i,f_i)/z[e_i]))
		
	#Same as update counts, but updates a dictionary mapping foreign words to a probability
	#Uses formula totals[f] = totals[f] + t(e|f)/z_e
	def update_totals(self, e_sentence, f_sentence, totals, z):
		for e_i, f_i in product(e_sentence, f_sentence):
			totals[f_i] += self.t.get(e_i, f_i) / z[e_i]

	#updates the probability for e to f for each word after updating counts and totals
	#Uses formula: c(e|f) / totals[f]
	def update_probabilities(self, counts, totals):
		for e_w, f_w in self.t.efdict.keys():
			e_i = self.t.e_vocab.get_int(e_w)
			f_i = self.t.f_vocab.get_int(f_w)
			self.t.set(e_i, f_i, counts.get(e_i,f_i)  / totals[f_i])

	#returns a totals dictionary with each total initially to 0
	def initialize_totals(self):
		f_vocab = self.t.f_vocab
		return {f_vocab.get_int(f_w): 0.0 for f_w in f_vocab.words()}

	def process_sentence_pair(self, sentence_index, counts, totals):
		e_sentence = self.parallel_corpus.get_e(sentence_index)
		f_sentence = self.parallel_corpus.get_f(sentence_index)
		
		z = self.compute_normalization(e_sentence, f_sentence)
		
		self.update_counts(e_sentence, f_sentence, counts, z)
		self.update_totals(e_sentence, f_sentence, totals, z)


	def expectation_maximization(self):
		counts = Conditional("count", 
							 self.parallel_corpus.e_vocab, 
							 self.parallel_corpus.f_vocab, 
							 0.0)

		totals = self.initialize_totals()

		for sentence_index in range(0, self.parallel_corpus.size()):
			self.process_sentence_pair(sentence_index, counts, totals)

		self.update_probabilities(counts, totals)



	def estimate_model(self, epsilon, delta, max_iterations=100, verbose=0):

		iterations = 0
		old_ppl = float("inf")
		new_ppl = self.parallel_corpus.perplexity(epsilon, self.t)

		while (old_ppl - new_ppl > delta and iterations < max_iterations):

			if verbose >= 1:
				print("Iteration {}: perplexity {} --> {}".format(iterations, old_ppl, new_ppl))
			if verbose >= 3 or (verbose >= 2 and iterations==0):
				print(self.t)

			self.expectation_maximization()

			old_ppl = new_ppl
			new_ppl = self.parallel_corpus.perplexity(epsilon, self.t)

			iterations += 1

		if verbose >= 1:
			print("Iteration {}: perplexity {} --> {}".format(iterations, old_ppl, new_ppl))
		if verbose >= 2:
			print(self.t)

#if ran directly with a filename argument, add translations separated by tabs to the model
#otherwise if ran directly, add a few sample translations (english - german)
#in both cases estimate the model and calculate conditional probabilities, then show the perplexity
#epsilon / delta can be changed
if __name__ == '__main__':
	
	corpus = ParallelCorpus()

	if len(sys.argv) > 1:
		f = open(sys.argv[1])
		for line in f:
			e,f = line.split("\t")
			corpus.add(e,f)

	else:
		corpus.add("the house", "das Haus")
		corpus.add("the book",  "das Buch")
		corpus.add("a book",	"ein Buch")

	model1 = IBM_Model1(corpus)
	epsilon = 0.01
	delta = 0.1

	model1.estimate_model(epsilon, delta, max_iterations=50, verbose=3)

	ppl = model1.parallel_corpus.perplexity(epsilon, model1.t)
	print()
	print(ppl)
