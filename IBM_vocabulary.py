#!/usr/bin/python3

import sys
import nltk
from collections import OrderedDict

# Implementation of vocabulary class for IBM model 1
class Vocabulary:

	# initialize word-index map / index-word map
	def __init__(self):
		self.word2int = OrderedDict()
		self.int2word = list()

	# Return the words stored in this vocabulary
	def words(self):
		return self.int2word

	# Given a word, return a unique integer for that word.
	# If a word has never been seen before, it assigns a new integer, and returns it.
	# The first word seen has value 0, then value 1, and so on.
	# If a word has previously been assigned an integer, it return that integer.
	def get_int(self, word):
		idx = -1
		if word not in self.word2int:
			idx = len(self.word2int.keys())
			self.word2int[word] = idx
			self.int2word.append(word)
		return self.word2int[word]


	# Given a non-negative integer that has been assigned to a word, returns the corresponding word.
	# Otherwise, returns the value None
	# (slow)
	def get_word(self, i):
		if i >= 0 and i in self.word2int.values():
			return self.int2word[i]
		else:
			return None

	# Return the number of words stored in this vocabulary
	def size(self):
		return len(self.int2word)

#if run directly with a filename argument, get the words from that file and show the words and indices
#otherwise does the same thing with a sample sentence
if __name__ == '__main__':

	if (len(sys.argv) > 1):
		f = open(sys.argv[1])
		raw = f.read()
		words=nltk.word_tokenize(raw)
	else:
		words = nltk.word_tokenize("All human beings are born free and equal in dignity and rights. They are endowed with reason and conscience and should act towards one another in a spirit of brotherhood.")
		
	v = Vocabulary()
		
	for word in words:
		i = v.get_int(word)
		w = v.get_word(i)
		print("{}\t{}\t{}".format(i, w, word))