# nltk_IBM_model1
(Implementation of HW1 in LING 402)

Implementation of IBM alignment model 1 in python and nltk, or simple lexical translation using conditional probabilities and a parallel corpus. Must have nltk available in python for import.

The main file is IBM_model1.py and IBM_parallelcorpus.py, which contain the IBM_Model1 and ParallelCorpus classes respectively. The parallel corpus needs to have native language and foreign language sentences added to it, and the IBM_Model1 must be initialized with a parallel corpus before it can estimate its model / probabilities. 

IBM_Model1 class' estimate_model method will calculate expectation maximization and update probabilities, and will update the probabilities of the conditional probability object counts within the parallel corpus. It will also print the parallel corpus perplexity and its estimated conditional probabilities after each iteration.

Part of sample output:

```
Iteration 1: perplexity 25.931568569324178 --> 23.731896224487812
t[the | das] = 0.5
t[the | Haus] = 0.5
t[the | Buch] = 0.25
t[the | ein] = 0.0
t[house | das] = 0.25
t[house | Haus] = 0.5
t[house | Buch] = 0.0
t[house | ein] = 0.0
t[book | das] = 0.25
t[book | Haus] = 0.0
t[book | Buch] = 0.5
t[book | ein] = 0.5
t[a | das] = 0.0
t[a | Haus] = 0.0
t[a | Buch] = 0.25
t[a | ein] = 0.5

Iteration 2: perplexity 23.731896224487812 --> 23.496082234019028
t[the | das] = 0.6363636363636364
t[the | Haus] = 0.4285714285714286
t[the | Buch] = 0.18181818181818182
t[the | ein] = 0.0
t[house | das] = 0.18181818181818182
t[house | Haus] = 0.5714285714285715
t[house | Buch] = 0.0
t[house | ein] = 0.0
t[book | das] = 0.18181818181818182
t[book | Haus] = 0.0
t[book | Buch] = 0.6363636363636364
t[book | ein] = 0.4285714285714286
t[a | das] = 0.0
t[a | Haus] = 0.0
t[a | Buch] = 0.18181818181818182
t[a | ein] = 0.5714285714285715
```
